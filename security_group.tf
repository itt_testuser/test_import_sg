resource "aws_security_group" "tfer--launch-wizard-1_sg-0d4233910b7df1b51" {
  description = "launch-wizard-1 created 2019-09-05T20:54:10.040+05:30"

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = "0"
    protocol    = "-1"
    self        = "false"
    to_port     = "0"
  }

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = "22"
    description = "ssh port"
    protocol    = "tcp"
    self        = "false"
    to_port     = "22"
  }

  name   = "launch-wizard-1"
  vpc_id = "vpc-f5c2c18e"
}
